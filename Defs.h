#ifndef DEFS_H
#define DEFS_H

#define LAMBDA '_'

#define STAR '*'
#define CONCAT '.'
#define UNION '+'
#define LPAR '('
#define RPAR ')'

typedef char Symbol;
typedef char* Word;

#endif