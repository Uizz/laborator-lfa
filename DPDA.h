#ifndef DPDA_H
#define DPDA_H

#include <stack>
#include "Automaton.h"

class DPDA : public Automaton
{
	set<Symbol> gamma;
	stack<Symbol> stack;
	Symbol Z0;

	void HandleStackOp(Word stackOp);

public:
	inline DPDA(StateSet* states, State* Q0, set<Symbol> sigma, set<Symbol> gamma, Symbol Z0)
		: Automaton(states, Q0, sigma) 
	{
		this->gamma = gamma;
		this->Z0 = Z0;
	};
	static DPDA ReadFromFile(string path);
	bool AcceptsWord(Word w);
};

#endif