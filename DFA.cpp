#include "DFA.h"
#include <fstream>

DFA DFA::ReadFromFile(string src)
{
	ifstream f(src);

	int nrOfStates;		f >> nrOfStates;
	int alphabetSize;	f >> alphabetSize;

	set<Symbol> alphabet;
	Symbol s;
	for (int i = 0; i < alphabetSize; i++)
	{
		f >> s;
		alphabet.insert(s);
	}

	StateSet* states = new StateSet(nrOfStates);

	int q0;		f >> q0;
	State* Q0 = states->StateWithID(q0);

	int nrF;	f >> nrF;
	for (int i = 0; i < nrF; i++)
	{
		int qF;		f >> qF;
		states->StateWithID(qF)->IsFinal = true;
	}

	while (!f.eof())
	{
		int qX;		f >> qX;
		State* qS = states->StateWithID(qX);
		Symbol s;	f >> s;
		SymbolicTransition* t = new SymbolicTransition(s);
		f >> qX;
		t->AddTransitionTo(states->StateWithID(qX));
		qS->AddSymbolicTransition(t);
	}
	f.close();
	return DFA(states, Q0, alphabet);
}

void DFA::WriteToFile(string dest)
{
	ofstream o(dest);
	o << this->states->GetNumberOfStates() << endl;
	o << this->alphabet.size() << " ";
	for (set<Symbol>::iterator it = alphabet.begin(); it != alphabet.end(); it++)
		o << *it << " ";
	o << endl;
	o << this->Q0->GetID() << endl;
	StateSet* finalStates = this->GetFinalStates();
	o << finalStates->GetNumberOfStates() << " ";
	for (StateSetIterator it = finalStates->StartOfSet(); it != finalStates->EndOfSet(); it++)
		o << (*it)->GetID() << " ";
	o << endl << endl;

	for (StateSetIterator it = this->states->StartOfSet(); it != this->states->EndOfSet(); it++)
	{
		if ((*it)->GetSymbolicTransitionSet()->GetTransitionsCount())
		{
			SymbolicTransitionSet* stSet = (*it)->GetSymbolicTransitionSet();
			for (SymbolicTransitionSetIterator it2 = stSet->StartOfSet(); it2 != stSet->EndOfSet(); it2++)
			{
				o << (*it)->GetID() << " ";
				o << (*it2)->GetSymbol() << " ";
				StateSet* qSet = (*it2)->GetStateSet();
				o << (*qSet)[0]->GetID() << endl;
			}
		}
	}
	o.close();
}