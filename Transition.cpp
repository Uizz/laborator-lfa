#include "Transition.h"

SymbolicTransition::SymbolicTransition(Symbol s) 
{
	this->s = s;
	this->qD = new StateSet(0);
}

void SymbolicTransition::AddTransitionTo(State* q)
{
	this->qD->AddState(q);
}

StateSet* SymbolicTransition::GetStateSet()
{
	return this->qD;
}

SymbolicTransition* SymbolicTransitionSet::GetTransitionForSymbol(Symbol s)
{
	SymbolicTransitionSetIterator i;
	for (i = transitions.begin(); i != transitions.end(); i++)
		if ((*i)->GetSymbol() == s)
			return *i;
	return NULL;
}

PushDownTransition::PushDownTransition(PDTInput input, State* finalState, Word stackOp)
{
	this->input = input;
	this->finalState = finalState;
	this->stackOp = stackOp;
}

PushDownTransition* PushDownTransitionSet::GetTransitionForInput(PDTInput input)
{
	for (set<PushDownTransition*>::iterator it = transitions.begin(); it != transitions.end(); it++)
		if ((*it)->GetInput() == input)
			return *it;
	return NULL;
}