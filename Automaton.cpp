#include "Automaton.h"

Automaton::Automaton(StateSet* states, State* Q0, set<Symbol> alphabet)
{
	this->states = states;
	this->Q0 = Q0;
	this->alphabet = alphabet;
}

StateSet* Automaton::GetFinalStates()
{
	StateSet* finalStates = new StateSet(0);
	for (StateSetIterator it = states->StartOfSet(); it != states->EndOfSet(); it++)
		if ((*it)->IsFinal)
			finalStates->AddState(*it);
	return finalStates;
}