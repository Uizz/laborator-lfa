#include <queue>
#include "State.h"

State::State(int id)
{
	this->id = id;
	this->IsFinal = false;
	this->symbolicTransitions = new SymbolicTransitionSet;
	this->pushDownTransitions = new PushDownTransitionSet;
}

void State::AddSymbolicTransition(SymbolicTransition* st)
{
	this->symbolicTransitions->AddSymbolicTransition(st);
}

void State::AddPushDownTransition(PushDownTransition* pdt)
{
	this->pushDownTransitions->AddPushDownTransition(pdt);
}

StateSet::StateSet(int nrOfStates)
{
	for (int i = 0; i < nrOfStates; i++)
	{
		State* q = new State(i);
		states.insert(q);
	}
}

void StateSet::AddStateSet(StateSet* qset)
{
	for (StateSetIterator it = qset->StartOfSet(); it != qset->EndOfSet(); it++)
		this->AddState(*it);
}

State* StateSet::StateWithID(int id)
{
	for (StateSetIterator i = states.begin(); i != states.end(); i++)
		if ((*i)->GetID() == id)
			return *i;
	return NULL;
}

State* StateSet::operator[](int i)
{
	int k = -1;
	for (StateSetIterator j = states.begin(); j != states.end(); j++)
	{
		k++;
		if (k == i)
			return *j;
	}
	return NULL;
}

bool StateSet::Contains(State* s)
{
	for (StateSetIterator i = states.begin(); i != states.end(); i++)
		if (*i == s)
			return true;
	return false;
}

StateSet* State::LambdaClosure()
{
	StateSet* lambdaClosure = new StateSet(0);

	bool *visited = new bool[MAX_SET_STATES];
	for (int i = 0; i < MAX_SET_STATES; i++)
		visited[i] = false;

	queue<State*> queue;
	State* crtState;

	visited[this->GetID()] = true;
	queue.push(this);

	while (!queue.empty())
	{
		crtState = queue.front();
		lambdaClosure->AddState(crtState);
		queue.pop();
		SymbolicTransition* lambdaTransitions = crtState->GetSymbolicTransitionSet()->GetTransitionForSymbol(LAMBDA);
		if (lambdaTransitions)
		{
			StateSet* qD = lambdaTransitions->GetStateSet();
			int n = qD->GetNumberOfStates();
			for (int i = 0; i < n; i++)
			{
				State* q = (*qD)[i];
				if (!visited[q->GetID()])
				{
					visited[q->GetID()] = true;
					queue.push(q);
				}
			}
		}
	}
	return lambdaClosure;
}

StateSet* StateSet::LambdaClosure()
{
	if (!this->GetNumberOfStates())
		return this;

	StateSet* lambdaClosure = new StateSet(0);

	bool *visited = new bool[MAX_SET_STATES];
	for (int i = 0; i < MAX_SET_STATES; i++)
		visited[i] = false;

	queue<State*> queue;
	State* crtState;

	for (StateSetIterator it = this->StartOfSet(); it != this->EndOfSet(); it++)
		queue.push(*it);

	visited[(*this->StartOfSet())->GetID()] = true;

	while (!queue.empty())
	{
		crtState = queue.front();
		lambdaClosure->AddState(crtState);
		queue.pop();
		SymbolicTransition* lambdaTransitions = crtState->GetSymbolicTransitionSet()->GetTransitionForSymbol(LAMBDA);
		if (lambdaTransitions)
		{
			StateSet* qD = lambdaTransitions->GetStateSet();
			int n = qD->GetNumberOfStates();
			for (int i = 0; i < n; i++)
			{
				State* q = (*qD)[i];
				if (!visited[q->GetID()])
				{
					visited[q->GetID()] = true;
					queue.push(q);
				}
			}
		}
	}
	return lambdaClosure;
}