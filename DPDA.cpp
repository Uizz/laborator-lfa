#include "DPDA.h"
#include <fstream>
#include <string>
#include <iostream>
#include <stack>
DPDA DPDA::ReadFromFile(string src)
{
	ifstream f(src);

	int nrOfStates;		f >> nrOfStates;
	StateSet* states = new StateSet(nrOfStates);

	set<Symbol> sigma, gamma;
	Symbol Z0, s;

	int alphabetSize;		f >> alphabetSize;
	for (int i = 0; i < alphabetSize; i++)
	{
		f >> s;
		sigma.insert(s);
	}
	
	f >> alphabetSize;
	for (int i = 0; i < alphabetSize; i++)
	{
		f >> s;
		gamma.insert(s);
		if (i == alphabetSize - 1) Z0 = s;
	}

	int q0;		f >> q0;
	State* Q0 = states->StateWithID(q0);

	int nrF;	f >> nrF;
	for (int i = 0; i < nrF; i++)
	{
		int qF;		f >> qF;
		states->StateWithID(qF)->IsFinal = true;
	}

	while (!f.eof())
	{
		int qS, qD;
		Symbol sigmaX, gammaX;
		string stackop;

		f >> qS >> sigmaX >> gammaX >> qD >> stackop;

		Word stackOp = new Symbol[stackop.size() + 1];
		copy(stackop.begin(), stackop.end(), stackOp);
		stackOp[stackop.size()] = 0;

		State* crtState = states->StateWithID(qS), *destState = states->StateWithID(qD);
		PushDownTransition* pdt = new PushDownTransition({ sigmaX, gammaX }, destState, stackOp);
		crtState->GetPushDownTransitionSet()->AddPushDownTransition(pdt);
	}
	return DPDA(states, Q0, sigma, gamma, Z0);
}

bool DPDA::AcceptsWord(Word w)
{
	this->stack.push(Z0);
	int wordIterator = 0, wordLength = strlen(w);
	State* crtState = this->Q0;
	PushDownTransition* pdt;
	do
	{
		if (stack.empty())
			return false;
		pdt = crtState->GetPushDownTransitionSet()->GetTransitionForInput({ w[wordIterator] , stack.top() });
		if (!pdt)
		{
			pdt = crtState->GetPushDownTransitionSet()->GetTransitionForInput({ LAMBDA, stack.top() });
			if (!pdt)
				return false;
		}
		else wordIterator++;
		HandleStackOp(pdt->GetStackOp());
		crtState = pdt->GetFinalState();
		cout << endl << "Cuvantul ramas : ";
		for (int i = wordIterator; i < wordLength; i++)
			cout << w[i];
		cout << "\tStiva : ";
		std::stack<Symbol> temp;
		while (!stack.empty())
		{
			temp.push(stack.top());
			cout << stack.top();
			stack.pop();
		}
		while (!temp.empty())
		{
			stack.push(temp.top());
			temp.pop();
		}
	} while (wordIterator < wordLength);
	while (pdt && !stack.empty())
	{
		pdt = crtState->GetPushDownTransitionSet()->GetTransitionForInput({ LAMBDA, stack.top() });
		if (pdt)
		{
			HandleStackOp(pdt->GetStackOp());
			crtState = pdt->GetFinalState();
		}
	}
	if (crtState->IsFinal)
		return true;
	else return false;
}

void DPDA::HandleStackOp(Word stackOp)
{
	stack.pop();
	if (stackOp[0] != '_')
	{
		for (int i = strlen(stackOp) - 1; i >= 0; i--)
			stack.push(stackOp[i]);
	}
}