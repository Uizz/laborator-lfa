#ifndef REGEX_CONVERTER
#define REGEX_CONVERTER

#include <stack>
#include "NFA.h"

typedef struct
{
	stack<Symbol> Characters, Operators;
	set<Symbol> Alphabet;
	NFA* NFA;
} RegexConversionStack;

class RegexConverter
{
private:
	static int stateCounter;
	static RegexConversionStack GenerateConversionStack(Word regex);
	static bool IsOperator(Symbol s);
	static NFA* BuildNFAForSymbol(Symbol s);
	static NFA* HandleConversionStack(RegexConversionStack& convStack);
	static NFA* HandleParanthesis(RegexConversionStack& convStack);
	static NFA* GetNextNFAFromSymbol(RegexConversionStack& convStack);
	static void Stelate(NFA* A);
	static void Concatenate(NFA* A, NFA* B);
	static void Unite(NFA* A, NFA* B);
public:
	static NFA* RegexToNFA(Word regex);
};

#endif