#ifndef DFA_H
#define DFA_H

#include "State.h"
#include "Automaton.h"

class DFA : public Automaton
{
public:
	DFA(StateSet* states, State* Q0, set<Symbol> alphabet) : Automaton(states, Q0, alphabet) {};
	static DFA ReadFromFile(string src);
	void WriteToFile(string dest);
};

#endif