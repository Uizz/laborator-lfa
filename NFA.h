#ifndef AFN_H
#define AFN_H

#include "State.h"
#include "Automaton.h"

class DFA;

class NFA : public Automaton
{
public:
	NFA(StateSet* states, State* Q0, set<Symbol> alphabet) : Automaton(states, Q0, alphabet) {};
	static NFA ReadFromFile(string src);
	void WriteToFile(string dest);
	bool AcceptsWord(Word word);
	DFA ToDFA();
}; 

#endif