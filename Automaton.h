#ifndef AUTOMATON_H
#define AUTOMATON_H

#include "State.h"
#include "Transition.h"

class Automaton
{
protected:
	StateSet* states;
	State* Q0;
	set<Symbol> alphabet;
public:
	Automaton(StateSet* states, State* Q0, set<Symbol> alphabet);
	inline set<Symbol> GetAlphabet() { return this->alphabet; }
	inline void AddSymbolToAlphabet(Symbol s) { this->alphabet.insert(s); }
	inline State* GetInitialState() { return this->Q0; }
	inline void SetInitialState(State* Q0) { this->Q0 = Q0; }
	inline StateSet* GetAllStates() { return this->states; }
	StateSet* GetFinalStates();
};

#endif