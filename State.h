#ifndef STATE_H
#define STATE_H

#include <set>
#include "Transition.h"

#define MAX_SET_STATES 128

using namespace std;

class StateSet;
class SymbolicTransition;
class SymbolicTransitionSet;
class PushDownTransition;
class PushDownTransitionSet;

class State
{
	int id;
	SymbolicTransitionSet* symbolicTransitions;
	PushDownTransitionSet* pushDownTransitions;

public:
	State(int id);
	inline int GetID() { return this->id; }
	void AddSymbolicTransition(SymbolicTransition* st);
	inline SymbolicTransitionSet* GetSymbolicTransitionSet() { return this->symbolicTransitions; }
	void AddPushDownTransition(PushDownTransition* st);
	inline PushDownTransitionSet* GetPushDownTransitionSet() { return this->pushDownTransitions; }
	StateSet* LambdaClosure();
	bool IsFinal;
};

typedef set<State*>::iterator StateSetIterator;

class StateSet
{
	set<State*> states;

public:
	StateSet(int nrOfStates);
	void AddState(State* q) { this->states.insert(q); }
	void AddStateSet(StateSet* qset);
	inline int GetNumberOfStates() { return states.size(); }
	StateSet* LambdaClosure();
	State* StateWithID(int ID);
	State* operator[](int i);
	bool Contains(State* s);
	inline StateSetIterator StartOfSet() { return this->states.begin(); }
	inline StateSetIterator EndOfSet() { return this->states.end(); }
	inline bool operator<(const StateSet& other) const
	{
		return states < other.states;
	}
};


#endif