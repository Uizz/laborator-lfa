#ifndef TRANSITION_H
#define TRANSITION_H

#include <set>
#include "Defs.h"
#include "State.h"
using namespace std;

class State;
class StateSet;

class SymbolicTransition
{
	Symbol s;
	StateSet* qD;

public:
	SymbolicTransition(Symbol s);
	inline Symbol GetSymbol() { return this->s; }
	void AddTransitionTo(State* q);
	StateSet* GetStateSet();
};

typedef set<SymbolicTransition*>::iterator SymbolicTransitionSetIterator;

class SymbolicTransitionSet
{
	set<SymbolicTransition*> transitions;

public:
	inline void AddSymbolicTransition(SymbolicTransition* st) { this->transitions.insert(st); }
	SymbolicTransition* GetTransitionForSymbol(Symbol s);
	inline int GetTransitionsCount() { return this->transitions.size(); }
	inline SymbolicTransitionSetIterator StartOfSet() { return this->transitions.begin(); }
	inline SymbolicTransitionSetIterator EndOfSet() { return this->transitions.end(); }
};

struct PDTInput
{
	Symbol Symbol, StackTop;
	bool operator==(PDTInput& i)
	{
		return this->Symbol == i.Symbol && this->StackTop == i.StackTop;
	}
};

class PushDownTransition
{
	PDTInput input;
	State* finalState;
	Word stackOp;

public:
	PushDownTransition(PDTInput input, State* finalState, Word stackOp);
	inline PDTInput GetInput() { return this->input; }
	inline State* GetFinalState() { return this->finalState; }
	inline Word GetStackOp() { return this->stackOp; }
};

class PushDownTransitionSet
{
	set<PushDownTransition*> transitions;

public:
	inline void AddPushDownTransition(PushDownTransition* pdt) { this->transitions.insert(pdt); }
	PushDownTransition* GetTransitionForInput(PDTInput input);
};

#endif