#include "RegexConverter.h"
#include "NFA.h"


bool RegexConverter::IsOperator(Symbol s)
{
	return s == STAR || s == CONCAT || s == UNION || s == LPAR || s == RPAR;
}

NFA* RegexConverter::RegexToNFA(Word regex)
{
	RegexConversionStack convStack = GenerateConversionStack(regex);
	return HandleConversionStack(convStack);
}

NFA* RegexConverter::HandleConversionStack(RegexConversionStack& convStack)
{
	NFA* A = NULL;
	while (!convStack.Operators.empty())
	{
		Symbol nextOperator = convStack.Operators.top();
		convStack.Operators.pop();

		RegexConversionStack tempRCS;

		switch (nextOperator)
		{
		case STAR:
			if (convStack.Operators.top() == RPAR)
			{
				convStack.Operators.pop();
				tempRCS = { convStack.Characters, convStack.Operators, convStack.Alphabet, NULL };
				convStack.NFA = HandleConversionStack(tempRCS);
				convStack.Operators = tempRCS.Operators;
				convStack.Characters = tempRCS.Characters;
			}
			else
				convStack.NFA = GetNextNFAFromSymbol(convStack);
			Stelate(convStack.NFA);
			break;
		case CONCAT:
			if (!convStack.NFA)
				convStack.NFA = GetNextNFAFromSymbol(convStack);
			A = HandleParanthesis(convStack);
			Concatenate(A, convStack.NFA);
			convStack.NFA = A;
			break;
		case UNION:
			if (!convStack.NFA)
				convStack.NFA = GetNextNFAFromSymbol(convStack);
			A = HandleParanthesis(convStack);
			Unite(A, convStack.NFA);
			convStack.NFA = A;
			break;
		case RPAR:
			tempRCS = { convStack.Characters, convStack.Operators, convStack.Alphabet, NULL };
			convStack.NFA = HandleConversionStack(tempRCS);
			convStack.Operators = tempRCS.Operators;
			convStack.Characters = tempRCS.Characters;
			break;
		case LPAR:
			return convStack.NFA;
		}
	}
	return convStack.NFA;
}

NFA* RegexConverter::HandleParanthesis(RegexConversionStack& convStack)
{
	RegexConversionStack tempRCS;
	NFA* tempNFA;
	if (convStack.Operators.top() == RPAR)
	{
		convStack.Operators.pop();
		tempRCS = { convStack.Characters, convStack.Operators, convStack.Alphabet, NULL };
		tempNFA = HandleConversionStack(tempRCS);
		convStack.Operators = tempRCS.Operators;
		convStack.Characters = tempRCS.Characters;
	}
	else
	{
		if (convStack.Operators.top() == STAR)
		{
			convStack.Operators.pop();

			if (convStack.Operators.top() == RPAR)
			{
				convStack.Operators.pop();
				tempRCS = { convStack.Characters, convStack.Operators, convStack.Alphabet, NULL };
				tempNFA = HandleConversionStack(tempRCS);
				convStack.Operators = tempRCS.Operators;
				convStack.Characters = tempRCS.Characters;
			}
			else
				tempNFA = GetNextNFAFromSymbol(convStack);
			Stelate(tempNFA);
		}
		else
			tempNFA = GetNextNFAFromSymbol(convStack);
	}
	return tempNFA;
}

NFA* RegexConverter::GetNextNFAFromSymbol(RegexConversionStack& convStack)
{
	Symbol s = convStack.Characters.top();
	convStack.Characters.pop();
	return BuildNFAForSymbol(s);
}

NFA* RegexConverter::BuildNFAForSymbol(Symbol s)
{
	set<Symbol> alphabet;
	alphabet.insert(s);

	State* q0 = new State(stateCounter++), *q1 = new State(stateCounter++);
	SymbolicTransition* st = new SymbolicTransition(s);
	st->AddTransitionTo(q1);
	q0->AddSymbolicTransition(st);
	q1->IsFinal = true;

	StateSet* states = new StateSet(0);
	states->AddState(q0);
	states->AddState(q1);

	return new NFA(states, q0, alphabet);
}

void RegexConverter::Stelate(NFA* A)
{
	State* q0 = new State(stateCounter++);

	SymbolicTransition *q0Lambda = new SymbolicTransition(LAMBDA);
	q0Lambda->AddTransitionTo(A->GetInitialState());
	q0->AddSymbolicTransition(q0Lambda);
	q0->IsFinal = true;

	StateSet* finalAStates = A->GetFinalStates();
	for (StateSetIterator it = finalAStates->StartOfSet(); it != finalAStates->EndOfSet(); it++)
	{
		(*it)->IsFinal = false;
		SymbolicTransition* withLambda = (*it)->GetSymbolicTransitionSet()->GetTransitionForSymbol(LAMBDA);
		if (!withLambda)
			withLambda = new SymbolicTransition(LAMBDA);
		withLambda->AddTransitionTo(q0);
		(*it)->AddSymbolicTransition(withLambda);
	}

	A->SetInitialState(q0);
	A->GetAllStates()->AddState(q0);
}

void RegexConverter::Concatenate(NFA* A, NFA* B)
{
	StateSet* finalAStates = A->GetFinalStates();

	for (StateSetIterator it = finalAStates->StartOfSet(); it != finalAStates->EndOfSet(); it++)
	{
		(*it)->IsFinal = false;
		SymbolicTransitionSet* transitions = (*it)->GetSymbolicTransitionSet();
		if (!transitions->GetTransitionForSymbol(LAMBDA))
			transitions->AddSymbolicTransition(new SymbolicTransition(LAMBDA));
		transitions->GetTransitionForSymbol(LAMBDA)->AddTransitionTo(B->GetInitialState());
	}

	set<Symbol> alphabet = B->GetAlphabet();
	for (set<Symbol>::iterator it = alphabet.begin(); it != alphabet.end(); it++)
		A->AddSymbolToAlphabet(*it);

	A->GetAllStates()->AddStateSet(B->GetAllStates());
}

void RegexConverter::Unite(NFA* A, NFA* B)
{
	State* q0 = new State(stateCounter++), *qF = new State(stateCounter++);

	SymbolicTransition* withLambda = new SymbolicTransition(LAMBDA);

	withLambda->AddTransitionTo(A->GetInitialState());
	withLambda->AddTransitionTo(B->GetInitialState());

	q0->AddSymbolicTransition(withLambda);
	qF->IsFinal = true;

	StateSet* finalAStates = A->GetFinalStates(),
		*finalBStates = B->GetFinalStates();

	for (StateSetIterator it = finalAStates->StartOfSet(); it != finalAStates->EndOfSet(); it++)
	{
		SymbolicTransitionSet* transitions = (*it)->GetSymbolicTransitionSet();
		if (!transitions->GetTransitionForSymbol(LAMBDA))
			transitions->AddSymbolicTransition(new SymbolicTransition(LAMBDA));
		transitions->GetTransitionForSymbol(LAMBDA)->AddTransitionTo(qF);
		(*it)->IsFinal = false;
	}

	for (StateSetIterator it = finalBStates->StartOfSet(); it != finalBStates->EndOfSet(); it++)
	{
		SymbolicTransitionSet* transitions = (*it)->GetSymbolicTransitionSet();
		if (!transitions->GetTransitionForSymbol(LAMBDA))
			transitions->AddSymbolicTransition(new SymbolicTransition(LAMBDA));
		transitions->GetTransitionForSymbol(LAMBDA)->AddTransitionTo(qF);
		(*it)->IsFinal = false;
	}

	set<Symbol> alphabet = B->GetAlphabet();
	for (set<Symbol>::iterator it = alphabet.begin(); it != alphabet.end(); it++)
		A->AddSymbolToAlphabet(*it);

	A->GetAllStates()->AddStateSet(B->GetAllStates());
	A->GetAllStates()->AddState(q0);
	A->GetAllStates()->AddState(qF);
	A->SetInitialState(q0);
}

RegexConversionStack  RegexConverter::GenerateConversionStack(Word regex)
{
	RegexConversionStack convStack;
	convStack.NFA = NULL;
	for (unsigned int i = 0; i < strlen(regex); i++)
		if (IsOperator(regex[i]))
			convStack.Operators.push(regex[i]);
		else
			convStack.Characters.push(regex[i]);
	return convStack;
}
