#include <fstream>
#include <queue>
#include <map>
#include "NFA.h"
#include "DFA.h"

NFA NFA::ReadFromFile(string src)
{
	ifstream f(src);

	int nrOfStates;		f >> nrOfStates;
	int alphabetSize;	f >> alphabetSize;

	set<Symbol> alphabet;
	Symbol s;
	for (int i = 0; i < alphabetSize; i++)
	{
		f >> s;
		alphabet.insert(s);
	}

	StateSet* states = new StateSet(nrOfStates);

	int q0;		f >> q0;
	State* Q0 = states->StateWithID(q0);

	int nrF;	f >> nrF;
	for (int i = 0; i < nrF; i++)
	{
		int qF;		f >> qF;
		states->StateWithID(qF)->IsFinal = true;
	}

	while (!f.eof())
	{
		int qX;		f >> qX;
		State* qS = states->StateWithID(qX);
		Symbol s;	f >> s;
		SymbolicTransition* t = new SymbolicTransition(s);
		int transitionsCount;	f >> transitionsCount;
		for (int i = 0; i < transitionsCount; i++)
		{
			f >> qX;
			t->AddTransitionTo(states->StateWithID(qX));
		}
		qS->AddSymbolicTransition(t);
	}
	f.close();
	return NFA(states, Q0, alphabet);
}

void NFA::WriteToFile(string dest)
{
	ofstream o(dest);
	o << this->states->GetNumberOfStates() << " " << endl;
	o << this->alphabet.size() << " ";
	for (set<Symbol>::iterator it = alphabet.begin(); it != alphabet.end(); it++)
		o << *it << " ";
	o << endl;
	o << this->Q0->GetID() << endl;
	StateSet* finalStates = this->GetFinalStates();
	o << finalStates->GetNumberOfStates() << " ";
	for (StateSetIterator it = finalStates->StartOfSet(); it != finalStates->EndOfSet(); it++)
		o << (*it)->GetID() << " ";
	o << endl << endl;

	for (StateSetIterator it = this->states->StartOfSet(); it != this->states->EndOfSet(); it++)
	{
		if ((*it)->GetSymbolicTransitionSet()->GetTransitionsCount())
		{
			SymbolicTransitionSet* stSet = (*it)->GetSymbolicTransitionSet();
			for (SymbolicTransitionSetIterator it2 = stSet->StartOfSet(); it2 != stSet->EndOfSet(); it2++)
			{
				if ((*it2)->GetStateSet()->GetNumberOfStates())
				{
					o << (*it)->GetID() << " ";
					o << (*it2)->GetSymbol() << " " << (*it2)->GetStateSet()->GetNumberOfStates() << " ";
					StateSet* qSet = (*it2)->GetStateSet();
					for (StateSetIterator it3 = qSet->StartOfSet(); it3 != qSet->EndOfSet(); it3++)
						o << (*it3)->GetID() << " ";
					o << endl;
				}
			}
		}
	}
	o.close();
}

DFA NFA::ToDFA()
{
	StateSet* states = new StateSet(0);
	map<StateSet, State*> stateForSet;

	int crtQ = 0;
	StateSet* lambdaClosure = Q0->LambdaClosure();
	stateForSet[*lambdaClosure] = new State(crtQ++);
	State* initialState = stateForSet[*lambdaClosure];
	states->AddState(initialState);
	
	queue<StateSet*> q;
	q.push(lambdaClosure);
	while (!q.empty())
	{
		StateSet* crtSet = q.front();
		State* crtState = stateForSet[*crtSet];
		q.pop();
		// for each symbol in alphabet
		for (set<Symbol>::iterator it = alphabet.begin(); it != alphabet.end(); it++)
		{
			// set of destination states from crtState for current symbol
			StateSet* destinationsWithCrtSymbol = new StateSet(0);
			// for each state in the corespondent NFA state set
			for (int j = 0; j < crtSet->GetNumberOfStates(); j++)
			{
				// get transitions with current symbol for each said state
				SymbolicTransition* st = (*crtSet)[j]->GetSymbolicTransitionSet()->GetTransitionForSymbol(*it);
				if (st)
				{
					// get destination states , if any
					StateSet* destinations = st->GetStateSet();
					// for each destination found
					for (int k = 0; k < destinations->GetNumberOfStates(); k++)
						// add it to the current state , if not already added
						if (!destinationsWithCrtSymbol->Contains((*destinations)[k]))
							destinationsWithCrtSymbol->AddState((*destinations)[k]);
				}
			}
			// get lambda closure of the destinations state set
			StateSet* s = destinationsWithCrtSymbol->LambdaClosure();
			// if the set isn't empty
			if (s->GetNumberOfStates())
			{
				// if there's no equivalent state in the DFA for that state set
				if (!stateForSet[*s])
				{
					// create a new one and add it to the DFA state set, also make it final if needed
					stateForSet[*s] = new State(crtQ++);
					states->AddState(stateForSet[*s]);
					for (StateSetIterator it = s->StartOfSet(); it != s->EndOfSet(); it++)
						if ((*it)->IsFinal)
							stateForSet[*s]->IsFinal = true;
					q.push(s);
				}
				// link crtState to the new destination state
				SymbolicTransition* st = new SymbolicTransition(*it);
				st->AddTransitionTo(stateForSet[*s]);
				crtState->AddSymbolicTransition(st);
			}
		}
	}
	return DFA(states, initialState, alphabet);
}

bool NFA::AcceptsWord(Word word)
{
	StateSet* resultingStates = new StateSet(0);
	StateSet* lambdaClosure = Q0->LambdaClosure();

	unsigned int wordIterator = 0;
	while (wordIterator < strlen(word))
	{
		StateSet* nextSet = new StateSet(0);
		for (StateSetIterator it = lambdaClosure->StartOfSet(); it != lambdaClosure->EndOfSet(); it++)
		{
			State* crtState = *it;
			SymbolicTransition* st = crtState->GetSymbolicTransitionSet()->GetTransitionForSymbol(word[wordIterator]);
			if (st)
			{
				StateSet* partialSet = st->GetStateSet();
				for (StateSetIterator it2 = partialSet->StartOfSet(); it2 != partialSet->EndOfSet(); it2++)
					nextSet->AddState(*it2);
			}
		}
		lambdaClosure = nextSet->LambdaClosure();
		wordIterator++;
	}
	for (StateSetIterator it = lambdaClosure->StartOfSet(); it != lambdaClosure->EndOfSet(); it++)
		if ((*it)->IsFinal)
			return true;
	return false;
}