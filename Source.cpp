#include <iostream>
#include "DFA.h"
#include "NFA.h"
#include "RegexConverter.h"
#include "DPDA.h"

using namespace std;

int RegexConverter::stateCounter = 0;

int main()
{
	//--------------------
	cout << "TEMA 1 ------------" << endl << endl;
	
	Word word = "bbbaaaaaabb";

	NFA nfa = NFA::ReadFromFile("nfa.txt");	// accepta (a+b)*.abb
	DFA dfa = DFA::ReadFromFile("dfa.txt");
	
	DFA dfaConverted = nfa.ToDFA();

	if (nfa.AcceptsWord(word))
		cout << "Cuvant acceptat";
	else cout << "Cuvant neacceptat.";

	dfaConverted.WriteToFile("dfa_converted.txt");

	//--------------------


	//--------------------
	cout << endl << endl << "TEMA 2 ------------" << endl << endl;

	Word regex = "(a+b+c)*.b";
	NFA* convertedNFA = RegexConverter::RegexToNFA(regex);
	convertedNFA->WriteToFile("nfa_regex.txt");

	Word w = "abcababcbddbd";
	if (convertedNFA->AcceptsWord(w))
		cout << "Cuvant acceptat";
	else cout << "Cuvant neacceptat";

	DFA regexAFD = convertedNFA->ToDFA();
	regexAFD.WriteToFile("dfa_regex.txt");

	//--------------------


	//--------------------
	cout << endl << endl << "TEMA 3 ------------" << endl << endl;

	DPDA dpda = DPDA::ReadFromFile("dpda.txt");	// accepta a^2n.b^3n.c^2k.b^k
	Word pdaWord = "aaaabbbbbbccccccddd";

	DPDA dpda2 = DPDA::ReadFromFile("dpda2.txt"); // accepta a^n.b^2n
	Word pdaWord2 = "aabbbb";

	cout << "DPDA 1 : ";
	if (dpda.AcceptsWord(pdaWord))
		cout << "Cuvant acceptat.";
	else cout << "Cuvant neacceptat";
	cout << endl << "DPDA 2 : ";
	if (dpda2.AcceptsWord(pdaWord2))
		cout << "Cuvant acceptat.";
	else cout << "Cuvant neacceptat";

	cout << endl << endl;

	system("PAUSE");
	return 0;
}